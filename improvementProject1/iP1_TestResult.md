>**本文档为改进方案1的IO性能测试报告**。
>实验分为两个部分，首先测试内核原始loop设备的IO性能，然后测试改进后的性能，最后对比两部分的测试数据得出结论
# 实验条件

**测试环境**：VBox虚拟机
**系统内核版本**：OpenEuler 4.19.90
**IO测试工具**：fio（版本3.7）

**fio命令参数**：
```bash
fio -filename=/mnt/loop_test_dir/test -direct=1 -iodepth 1 -thread -rw=randrw -ioengine=psync -bs=4k -size=1G -numjobs=10 -runtime=1000 -group_reporting -name=mytest
```

## 1.原始loop设备测试记录
重复测试三次然后取平均值
**第一次测试**：

```bash
read: IOPS=3725, BW=14.6MiB/s (15.3MB/s)(5118MiB/351699msec)
    clat (usec): min=5, max=463224, avg=1518.95, stdev=3936.07
     lat (usec): min=5, max=463224, avg=1519.13, stdev=3936.09
write: IOPS=3728, BW=14.6MiB/s (15.3MB/s)(5122MiB/351699msec)
    clat (usec): min=6, max=462862, avg=1158.87, stdev=3587.20
     lat (usec): min=7, max=462862, avg=1159.13, stdev=3587.23
```

**第二次测试**：

```bash
read: IOPS=4308, BW=16.8MiB/s (17.6MB/s)(5118MiB/304106msec)
    clat (usec): min=5, max=178447, avg=1325.86, stdev=3371.27
     lat (usec): min=5, max=178447, avg=1326.01, stdev=3371.30
write: IOPS=4312, BW=16.8MiB/s (17.7MB/s)(5122MiB/304106msec)
    clat (usec): min=6, max=104865, avg=988.18, stdev=3099.81
     lat (usec): min=7, max=104865, avg=988.42, stdev=3099.87
```

**第三次测试**：

```bash
read: IOPS=4472, BW=17.5MiB/s (18.3MB/s)(5118MiB/292906msec)
    clat (usec): min=5, max=2177.7k, avg=1288.81, stdev=6327.66
     lat (usec): min=5, max=2177.7k, avg=1289.00, stdev=6327.67
write: IOPS=4476, BW=17.5MiB/s (18.3MB/s)(5122MiB/292906msec)
    clat (usec): min=7, max=2165.2k, avg=940.34, stdev=3986.31
     lat (usec): min=7, max=2165.2k, avg=940.59, stdev=3986.34
```

## 2.改进后loop设备测试记录
重复测试三次然后取平均值
**第一次测试**：

```bash
read: IOPS=4536, BW=17.7MiB/s (18.6MB/s)(5118MiB/288768msec)
    clat (usec): min=6, max=726850, avg=2018.36, stdev=5442.03
     lat (usec): min=6, max=726850, avg=2018.53, stdev=5442.05
write: IOPS=4541, BW=17.7MiB/s (18.6MB/s)(5122MiB/288768msec)
    clat (usec): min=6, max=246079, avg=179.43, stdev=460.11
     lat (usec): min=6, max=246079, avg=179.66, stdev=460.18
```

**第二次测试**：

```bash
read: IOPS=4881, BW=19.1MiB/s (19.0MB/s)(5118MiB/268407msec)
    clat (usec): min=5, max=346706, avg=1820.92, stdev=4697.03
     lat (usec): min=5, max=346706, avg=1821.11, stdev=4697.05
write: IOPS=4885, BW=19.1MiB/s (20.0MB/s)(5122MiB/268407msec)
    clat (usec): min=7, max=28593, avg=220.59, stdev=530.56
     lat (usec): min=7, max=28594, avg=220.88, stdev=530.74
```

**第三次测试**：

```bash
read: IOPS=4687, BW=18.3MiB/s (19.2MB/s)(5118MiB/279500msec)
    clat (usec): min=5, max=1123.7k, avg=1927.48, stdev=5514.81
     lat (usec): min=5, max=1123.7k, avg=1927.66, stdev=5514.83
write: IOPS=4691, BW=18.3MiB/s (19.2MB/s)(5122MiB/279500msec)
    clat (usec): min=6, max=42375, avg=197.86, stdev=460.14
     lat (usec): min=6, max=42375, avg=198.13, stdev=460.27
```

## 3.测试结果对比分析
### (1) 提取测试记录关键指标取平均值

原始loop设备IO性能测试均值：
```bash
read: IOPS=4168, BW=16.3MiB/s, clat=1377.87us, lat=1378.05us
write: IOPS=4172, BW=16.3MiB/s, clat=1029.13us, lat=1,029.38us
```
改进后loop设备IO性能测试均值：

```bash
read: IOPS=4701, BW=18.36MiB/s, clat=1922.25us, lat=1922.43us
write: IOPS=4706, BW=18.36MiB/s, clat=199.29us, lat=199.55us
```

>**测试结果数据项说明**:
IOPS：每秒钟的IO数
BW：IO带宽
clat：完成延迟, 提交该IO请求到kernel后，处理所花的时间
lat：响应时间

### (2) 测试结果
改进后每秒钟的IO数和带宽平均提升13%，读取数据时延迟裂化60%但是写入数据延迟显著降低（-81%）

### (3) 测试结果原因分析
可以看到，由于增加了一个工作线程，使得loop设备整体得到的CPU时间片资源增加，内核调度到loop设备的IO工作线程的概率也增加了，因此改进后每秒钟的IO数和带宽得到了提升。

但是关于读取数据相关性能指标为何裂化如此严重尚不清楚原因，有待后续分析和改进。
### (4) 后续计划
在本方案中提供了一个内置的固定解决方式，用户只能被动接受，而考虑到loop设备的IO是一个开放性问题，可将loop设备的IO请求处理方式和线程数量交由用户自己根据需求决定，因此后续拟通过用户可配置化的方式实现loop设备的IO提交的并行化，使用户可以通过自己指定相关参数来灵活实现需求。