#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#define LOOP_CLOSE_PARALLEL  0x4C84

int main()
{
    int fd = 0;
    int cmd;
    int arg = 0;


    /*打开设备文件*/
    fd = open("/dev/loop0",O_RDWR);
    if (fd < 0)
    {
        printf("Open Dev loop0 Error!\n");
        return -1;
    }

    /* 调用命令LOOP_CLOSE_PARALLEL */
    printf("<--- 关闭loop设备IO并行开关 --->\n");
    cmd = LOOP_CLOSE_PARALLEL;
    if (ioctl(fd, cmd, &arg) < 0)
        {
            printf("关闭失败\n");
            return -1;
    }
    close(fd);
    return 0;
}

